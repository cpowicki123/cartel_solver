import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import scipy.optimize
import math
import OneShotGameFunctions as OSG

# Intercept of demand curve.
r = 10.0
# Marginal cost of quantity.
c = 1.0
# Cost of violence.
eta = 0.02
# Number of traffickers to kill.
kappa = 0.3
# The mixing threshold
mix_thresh = ((1/9) * (r - c) ** 2 + eta - c * kappa) / c
# Precision Constant
epsilon = 0.00000000000001
# infinity
infinity = float("inf")

zeta = 0.1
# Discount factor
delta = 0.8
# Periods of Punishment
T = 2

C0 = (5 * r - 2 * c) / 12
C1 = ((r + 2 * c) ** 2) / 144


class action:

    def __init__(self, a, q, s):
        self.alpha = a
        self.qi = q
        self.si = s

    def to_string(self):
        return "(alpha = " + str(self.alpha) + ", qi = " + str(self.qi) + ", si = " + str(self.si) + ")"

    def is_equal(self, action):
        if self.alpha == action.alpha and self.qi == action.qi:
            if self.si == action.si:
                return True
        return False


def calc_price(action, g):
    return max(r - 2 * (action.qi - action.si * kappa - g), 0.0)


def calc_payoff(action, g):
    price = calc_price(action, g)
    payoff = price * (action.qi - action.si * kappa - g) - c * action.qi - action.si * eta
    return max(payoff, 0.0)


def calculate_nash_values(gset):
    payoffs = {}
    alphas = {}
    quants = {}
    for g in gset:
        alpha = OSG.calculate_nash_alpha(g, 1, 0.0, 1.0)
        payoff = OSG.calculate_payoff(g, alpha)
        payoffs[g] = max(0.0, payoff)
        alphas[g] = alpha
        quants[g] = OSG.calculate_q(g, alpha)
    return payoffs, alphas, quants


def f(theta):
    x = scipy.stats.norm.pdf((math.log(theta) + 0.5 * zeta ** 2) / zeta)
    return x / (zeta * theta)


def F(theta):
    x = scipy.stats.norm.cdf((math.log(theta) + 0.5 * zeta ** 2) / zeta)
    return x


def lambda_func(theta):
    return (delta * f(theta) * theta) / (6 - 6 * delta)


def psi(theta, delta_V):
    return C0 - math.sqrt(C1 + lambda_func(theta) * delta_V)


def V_tilde(w, nash):
    return (1 - (delta ** T)) * nash + (delta ** T) * w


def RHS(theta, delta_v):
    x = 4 * psi(theta, delta_v) - r + c
    denom = (12 * zeta ** 2) * math.sqrt(C1 + lambda_func(theta) * delta_v)
    full_frac = ((-1) * x * (math.log(theta) + 0.5 * zeta ** 2)) / denom
    return full_frac - 1


def b_hat(w, g, nash):
    delta_v = w - V_tilde(w, nash)
    theta_interior, result = scipy.optimize.brentq(
        RHS, epsilon, math.exp(-0.5 * zeta ** 2), args=(delta_v), full_output=True)
    if result.converged:
        if 0 >= (1 - delta) * (kappa * psi(theta_interior, delta_v) - eta) - delta * delta_v:
            si_bar = 0
        else:
            si_bar = 1
        q_interior = psi(theta_interior, delta_v) + g + kappa * si_bar
        eq_action = action(1, q_interior, si_bar)
        comp1 = (1 - delta) * calc_payoff(eq_action, g) + \
            (delta * ((1 - F(theta_interior)) * w + F(theta_interior) * V_tilde(w, nash)))
        comp2 = (1 - delta) * nash + delta * w
        v = max(comp1, comp2)
        return v, (theta_interior, eq_action)
    else:
        print("Did not converge: ", w, g, nash)
        v = (1 - delta) * nash + delta * w  # Why are we doing this?
    return v


def calc_avg_disc_violence(action, theta_bar, g):
    V_Nash = (oneshot_nash_alphas[g] ** 2) * 2 * kappa
    numerator = (1 - delta) * 2 * kappa * action.si + delta * \
        F(theta_bar) * (1 - (delta ** T)) * V_Nash
    denominator = 1 - (delta * (1 - F(theta_bar))) - (F(theta_bar) * (delta ** (T + 1)))
    return numerator / denominator


def calc_q_hat(action, g):
    return action.qi - action.si * kappa - g


def calc_avg_disc_q_hat(action, theta_bar, g):
    Q_Nash = (2 * oneshot_nash_alphas[g]) * \
        (oneshot_nash_quantities[g] - g - 2 * oneshot_nash_alphas[g] * kappa)
    Q_hat = 2 * calc_q_hat(action, g)
    numerator = (1 - delta) * Q_hat + delta * F(theta_bar) * (1 - (delta ** T)) * Q_Nash
    denominator = 1 - (delta * (1 - F(theta_bar))) - (F(theta_bar) * (delta ** (T + 1)))
    return numerator / denominator


# Main Routine
g_grid = np.arange(0.0, 25.0, 0.5)
oneshot_nash_payoffs, oneshot_nash_alphas, oneshot_nash_quantities = calculate_nash_values(g_grid)
bounds = []
eq_theta = []
eq_violence = []
eq_qhat = []
for g in g_grid:
    monopoly_action = action(1, 0.25 * (r - c) + g, 0)
    w = calc_payoff(monopoly_action, g)
    found_lb = False
    while not found_lb:
        diff = infinity
        while diff >= epsilon:  # epsilon?
            v, outcome = b_hat(w, g, oneshot_nash_payoffs[g])
            diff = w - v
            w = v
        w_ub = w
        w_lb = w - epsilon
        check, misc = b_hat(w_lb, g, oneshot_nash_payoffs[g])
        if check >= w_lb:
            found_lb = True
        else:
            w = w_lb
            epsilon /= 10
    print(g, w_ub, w_lb, oneshot_nash_payoffs[g])
    bounds.append((w_ub, w_lb))
    eq_theta.append(outcome[0])
    eq_violence.append(calc_avg_disc_violence(outcome[1], outcome[0], g))
    eq_qhat.append(calc_avg_disc_q_hat(outcome[1], outcome[0], g))

# Plot F(Theta_Bar)
plt.plot(g_grid, [F(i) for i in eq_theta])
plt.xlabel("Arrests/firm g")
plt.ylabel("F(theta)")
plt.title("Probability of Triggering Punishment")
plt.grid(True)
plt.xticks(np.arange(0.0, 26.0, 2))
plt.savefig("F_Theta_Bar.png")

# Plot Average Discounted Violence
plt.clf()
plt.plot(g_grid, eq_violence)
plt.xlabel("Arrests/firm g")
plt.ylabel("Violence")
plt.title("Average Discounted Violence")
plt.grid(True)
plt.xticks(np.arange(0.0, 26.0, 2))
plt.savefig("Violence.png")

# Plot Average Discounted Quantity Delivered
plt.clf()
plt.plot(g_grid, eq_qhat)
plt.xlabel("Arrests/firm g")
plt.ylabel("Q Delivered")
plt.title("Average Discounted Quantity Delivered")
plt.grid(True)
plt.xticks(np.arange(0.0, 26.0, 2))
plt.savefig("Quantity.png")

# Plot Payoffs
plt.clf()
plt.plot(g_grid, [i[1] for i in bounds])
plt.xlabel("Arrests/firm g")
plt.ylabel("Payoffs")
plt.title("Payoffs")
plt.grid(True)
plt.xticks(np.arange(0.0, 26.0, 2))
plt.savefig("Payoffs.png")
