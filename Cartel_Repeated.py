import numpy as np
import matplotlib.pyplot as plt
import action
import scipy.stats
import OneShotGameFunctions as OSG
import math

# Intercept of demand curve.
r = 10.0
# Marginal cost of quantity.
c = 1.0
# Cost of violence.
eta = 0.02
# Number of traffickers to kill.
kappa = 0.3
# The mixing threshold
mix_thresh = ((1/9) * (r - c) ** 2 + eta - c * kappa) / c
# Precision Constant
epsilon = 0.00000000000001
# g Values
g_array = np.arange(0.0, 30.0, 0.1)
# infinity
infinity = float("inf")

zeta = 0.1
# Discount factor
delta = 0.8
# Periods of Punishment
T = 2
Q_Grid = np.arange(0.0, 15, 0.1)
P_Grid = np.arange(0.1, r, 0.1)
A_Grid = np.arange(0.0, 1.1, 0.3)


class action:

    def __init__(self, a, q, s):
        self.alpha = a
        self.qi = q
        self.si = s

    def to_string(self):
        return "(alpha = " + str(self.alpha) + ", qi = " + str(self.qi) + "si = " + str(self.si) + ")"

    def is_equal(self, action):
        if self.alpha == action.alpha and self.qi == action.qi:
            if self.si == action.si:
                return True
        return False


def f(theta):
    if theta == 0 or theta == infinity:
        return 1.0
    else:
        # x = scipy.stats.norm.pdf((math.log(theta) + 0.5 * zeta ** 2) / zeta
        x = 2.0
        return x / (zeta * theta)


def repeated_eqbs(G_Grid):
    equilibria = []
    num_g = 0.0
    for g in G_Grid:
        print("Iteration # %d, %f Complete" % (num_g, ((num_g/len(G_Grid)) * 100)))
        best_payoff = (-1) * infinity
        best_eqbm = None
        Nash_alpha = OSG.calculate_nash_alpha(g, 1.0, 0.0, 1.0)
        Nash_Payoff = OSG.calculate_payoff(g, Nash_alpha)
        for pbar_index in range(len(P_Grid)):
            for a in A_Grid:
                for q in Q_Grid:
                    for s in range(2):
                        action_prof = action(a, q, s)
                        price = calculate_price(g, action_prof)
                        Prob = f(P_Grid[pbar_index]/price)
                        lt_pbar = sum([f(i/price) for i in P_Grid[0: pbar_index]])
                        vreward = Calc_V_Reward(g, action_prof, Prob, Nash_Payoff)
                        vpunish = (1 - delta ** T) * Nash_Payoff + (delta**T) * vreward
                        if check_incentive_compatible(g, action_prof, lt_pbar, vpunish, vreward):
                            if best_payoff < vreward:
                                best_payoff = vreward
                                best_eqbm = (g, P_Grid[pbar_index], action_prof.to_string())
                                # print(best_payoff)
        equilibria += [(best_payoff, best_eqbm)]
        num_g += 1
        # print(equilibria)
    return equilibria


def check_incentive_compatible(g, ap, prob_lt, vp, vr):
    for a_prime in A_Grid:
        for q_prime in Q_Grid:
            for s_prime in range(2):
                dev_action = action(a_prime, q_prime, s_prime)
                if ap.is_equal(dev_action):
                    continue
                else:
                    dev_pay = (1 - delta) * calc_dev_payoff(g, ap, dev_action)
                    if dev_action.si != ap.si:
                        dev_pay += delta * vp
                    else:
                        dev_pay += delta * (prob_lt * vp + (1 - prob_lt) * vr)
                    if dev_pay > vr:
                        return False
    return True


def calculate_price(g, ap):
    in_price = (r - 2 * (ap.qi - g - (ap.si * kappa))) * ap.alpha
    out_price = (r - ap.qi + g) * 1 - ap.alpha
    return max(in_price + out_price, 0.1)


def Calc_V_Reward(g, a, f_shock, u_nash):
    numerator = (1 - delta) * action_payoff(g, a) + delta * (1 - delta**T) * f_shock * u_nash
    denominator = 1 - delta * (1 - f_shock) - (delta ** (T + 1)) * f_shock
    return numerator / denominator


def action_payoff(g, a):
    in_price = r - 2 * (a.qi - g - (a.si * kappa))
    in_utility = in_price * (a.qi - g - (a.si * kappa)) - c * a.qi - eta * a.si
    out_price = r - a.qi + g
    out_utility = out_price * (a.qi - g) - c * a.qi
    return (a.alpha * in_utility) + ((1 - a.alpha) * out_utility)


def calc_dev_payoff(g, a, dev_a):

    in_price = r - a.qi - dev_a.qi + 2 * g + (dev_a.si + a.si) * kappa
    in_utility = in_price * (dev_a.qi - g - (a.si * kappa))
    in_utility -= c * dev_a.qi + dev_a.si * eta

    out_price = r - dev_a.qi + g
    out_utility = out_price * (dev_a.qi - g) - c * dev_a.qi
    return (a.alpha * in_utility) + ((1 - a.alpha) * out_utility)


# Main
equibs = repeated_eqbs(np.arange(0.0, 25.0, 0.5))
print(equibs)
