set xlabel 'gtilde'
set ylabel 'gbar'
r = 10.
c = 1.
qm = (r - c) / 4.
qN = (r - c) / 3.

kappa = 0.3
qm = (r-c)/4.
eta = 0.02
sigma = 0.1

C0 = (5*r - 2*c) / 12.
C1 = (r + 2*c)**2 / 144.
