import numpy as np

import plotly as py
import plotly.graph_objs as go
import matplotlib.pyplot as plt

import scipy.stats
import scipy.optimize
import math

thetas = [i for i in np.arange(0.01, 1.2, 0.05)]

# Intercept of demand curve.
r = 10.0
# Marginal cost of quantity.
c = 1.0
# Number of traffickers to kill.
kappa = 0.3
# Cost of violence.
eta = 0.02 * kappa * (.25 * (r - c) - kappa)
# The mixing threshold
mix_thresh = ((1/9) * (r - c) ** 2 + eta - c * kappa) / c
# Precision Constant
epsilon = 0.00000000000001
# infinity
infinity = float("inf")
zeta = 0.1
# Discount factor
delta = 0.95
# Periods of Punishment
T = 20

C0 = (5 * r - 2 * c) / 12
C1 = ((r + 2 * c) ** 2) / 144


class action:

    def __init__(self, a, q, s, n=False):
        self.alpha = a
        self.qi = q
        self.si = s
        self.isnash = n

    def to_string(self):
        if self.isnash:
            return "NASH: (alpha = " + str(self.alpha) + ", qi = " + str(self.qi) + ", si = " + str(self.si) + ")"
        else:
            return "(alpha = " + str(self.alpha) + ", qi = " + str(self.qi) + ", si = " + str(self.si) + ")"

    def is_equal(self, action):
        if self.alpha == action.alpha and self.qi == action.qi:
            if self.si == action.si:
                return True
        return False


def calc_price(action, g):
    return max(r - 2 * (action.qi - action.si * kappa - g), 0.0)


def calc_payoff(action, g):
    price = calc_price(action, g)
    payoff = price * (action.qi - action.si * kappa - g) - c * action.qi - action.si * eta
    return max(payoff, 0.0)


def calculate_nash_values(gset):
    payoffs = {}
    alphas = {}
    quants = {}
    for g in gset:
        alpha = calculate_nash_alpha(g, 1.0, 0, 1)
        payoff = calculate_payoff(g, alpha)
        payoffs[g] = max(0.0, payoff)
        alphas[g] = alpha
        quants[g] = calculate_q(g, alpha)
    return payoffs, alphas, quants


def f(theta):
    if theta <= 0:
        return 0
    x = scipy.stats.norm.pdf((math.log(theta) + 0.5 * zeta ** 2) / zeta)
    return x / (zeta * theta)


def F(theta):
    if theta <= 0:
        return 0
    if theta == infinity:
        return 1
    x = scipy.stats.norm.cdf((math.log(theta) + 0.5 * zeta ** 2) / zeta)
    return x


def lambda_func(theta):
    return (delta * f(theta) * theta) / (6 - 6 * delta)


def psi(theta, delta_V):
    return C0 - math.sqrt(C1 + lambda_func(theta) * delta_V)


def V_tilde(w, nash):
    return (1 - (delta ** T)) * nash + (delta ** T) * w


def RHS(theta, delta_v):
    x = 4 * psi(theta, delta_v) - r + c
    denom = (12 * zeta ** 2) * math.sqrt(C1 + lambda_func(theta) * delta_v)
    full_frac = ((-1) * x * (math.log(theta) + 0.5 * zeta ** 2)) / denom
    return full_frac - 1


def b_hat(w, g, nash):
    delta_v = w - V_tilde(w, nash)
    theta_interior, result = scipy.optimize.brentq(
        RHS, epsilon, math.exp(-0.5 * zeta ** 2), args=(delta_v), full_output=True)
    if result.converged:
        if 0 >= (1 - delta) * (kappa * psi(theta_interior, delta_v) - eta) - delta * delta_v:
            si_bar = 0
        else:
            si_bar = 1
        q_interior = psi(theta_interior, delta_v) + g[0] + kappa * si_bar
        eq_action = action(1, q_interior, si_bar)
        comp1 = (1 - delta) * calc_payoff(eq_action, g[0]) + \
            (delta * ((1 - F(theta_interior)) * w + F(theta_interior) * V_tilde(w, nash)))
        comp2 = (1 - delta) * nash + delta * w
        if comp1 > comp2:
            return comp1, theta_interior, eq_action
        else:
            return comp2, 0.0, action(oneshot_nash_alphas[g[0]], oneshot_nash_quantities[g[0]], 1, n=True)
    else:
        print("Did not converge: ", w, g, nash)
        v = (1 - delta) * nash + delta * w
        return v, 0, action(oneshot_nash_alphas[g[1]], oneshot_nash_quantities[g[1]], 1)


def calc_avg_disc_violence(action, theta_bar, g):
    V_Nash = (oneshot_nash_alphas[g] ** 2) * 2 * kappa
    numerator = (1 - delta) * 2 * kappa * action.si * (action.alpha ** 2) + \
        delta * F(theta_bar) * (1 - (delta ** T)) * V_Nash
    denominator = 1 - (delta * (1 - F(theta_bar))) - (F(theta_bar) * (delta ** (T + 1)))
    return numerator / denominator


def calc_avg_disc_q_hat(action, theta_bar, g_bar, g_tilde):
    Q_hat = (2 * action.alpha) * (action.qi - g_bar - (action.alpha * action.si * kappa))
    Q_Nash = (2 * oneshot_nash_alphas[g_tilde]) * (oneshot_nash_quantities[g_tilde] -
                                                   g_tilde - oneshot_nash_alphas[g_tilde] * kappa)
    numerator = (1 - delta) * Q_hat + delta * F(theta_bar) * (1 - (delta ** T)) * Q_Nash
    denominator = 1 - (delta * (1 - F(theta_bar))) - (F(theta_bar) * (delta ** (T + 1)))
    if g_bar == 10:
        print(action.to_string(), theta_bar, g_bar, g_tilde)
        print(Q_Nash, Q_hat)
        print(denominator)
    return numerator / denominator


def calculate_nash_alpha(g, alpha, lb, ub):
    payoff = calculate_payoff(g, alpha)
    if g <= mix_thresh:
        return 1.0
    if (ub - lb) <= epsilon:
        return alpha
    if payoff < 0:
        return calculate_nash_alpha(g, (lb + ub) / 2, lb, alpha)
    elif payoff > 0:
        return calculate_nash_alpha(g, (lb + ub) / 2, alpha, ub)
    else:
        return alpha


def calculate_q(g, alpha):
    if alpha == 0:
        return 0
    else:
        return (r - c + ((2 + alpha) * g) + (3 * alpha * kappa)) / (2 + alpha)


def calculate_payoff(g, alpha):
    qi = calculate_q(g, alpha)

    # Calculate the utility of the case where both Cartels are 'In'
    in_price = r - 2 * (qi - g - kappa)
    in_utility = in_price * (qi - g - kappa) - c * qi - eta
    in_utility = in_utility * alpha

    # Calculate the utility of the case where one Cartel is In and the other is
    # 'Out'
    out_price = r - qi + g
    out_utility = out_price * (qi - g) - c * qi
    out_utility = out_utility * (1 - alpha)
    return in_utility + out_utility


# Main Routine
g_grid = np.arange(0.0, 22.5, 2.0)
X, Y = np.meshgrid(g_grid, g_grid)
oneshot_nash_payoffs, oneshot_nash_alphas, oneshot_nash_quantities = calculate_nash_values(g_grid)
bounds = np.zeros(shape=X.shape, dtype=float)
eq_theta = np.zeros(shape=X.shape, dtype=float)
eq_violence = np.zeros(shape=X.shape, dtype=float)
eq_qhat = np.zeros(shape=X.shape, dtype=float)

# for g_bar in g_grid:
row_num = 0
for i in range(len(g_grid)):
    col_num = i
    for g_tilde in g_grid[i:]:
        g_bar = g_grid[i]
        g_tuple = (g_bar, g_tilde)
        print(g_tuple)
        if g_bar == 10:
            plt.ion()
            plt.show()
        monopoly_action = action(1, 0.25 * (r - c) + g_bar, 0)
        w = calc_payoff(monopoly_action, g_bar)
        found_lb = False
        while not found_lb:
            diff = infinity
            while diff >= epsilon:
                v, theta, eq_action = b_hat(w, g_tuple, oneshot_nash_payoffs[g_tilde])
                diff = w - v
                w = v
            w_ub = w
            w_lb = w - epsilon
            check, misc, misc2 = b_hat(w_lb, g_tuple, oneshot_nash_payoffs[g_tilde])
            if check >= w_lb:
                found_lb = True
            else:
                w = w_lb
                epsilon /= 1.5
        bounds[row_num][col_num] = w_ub
        eq_theta[row_num][col_num] = F(theta)
        eq_violence[row_num][col_num] = calc_avg_disc_violence(eq_action, theta, g_tuple[1])
        eq_qhat[row_num][col_num] = calc_avg_disc_q_hat(
            eq_action, theta, g_tuple[0], g_tuple[1])
        col_num += 1
    row_num += 1

# Plot Avg Disc Violence
surf = [go.Surface(
    colorscale='Viridis',
    x=g_grid,
    y=g_grid,
    z=eq_violence
)]
layout = go.Layout(
    title="Average Discounted Violence",
    autosize=True,
    margin=dict(
        l=65,
        r=50,
        b=65,
        t=90
    ),
    scene=dict(
        xaxis=dict(
            title="G Tilde"
        ),
        yaxis=dict(
            title="G Bar"
        ),
        zaxis=dict(
            title="Avg. Disc. Violence"
        )
    )
)
fig = go.Figure(data=surf, layout=layout)
py.offline.plot(fig, filename="Violence3D.html")

# Plot F(Theta)
surf = [go.Surface(
    colorscale='Viridis',
    x=g_grid,
    y=g_grid,
    z=eq_theta
)]
layout = go.Layout(
    title="Probability of Triggering Punishment",
    autosize=True,
    margin=dict(
        l=65,
        r=50,
        b=65,
        t=90
    ),
    scene=dict(
        xaxis=dict(
            title="G Tilde"
        ),
        yaxis=dict(
            title="G Bar"
        ),
        zaxis=dict(
            title="F(Theta)"
        )
    )
)
fig = go.Figure(data=surf, layout=layout)
py.offline.plot(fig, filename="FThetaBar3D.html")

# Plot Quantity Delivered
surf = [go.Surface(
    colorscale='Viridis',
    x=g_grid,
    y=g_grid,
    z=eq_qhat
)]
layout = go.Layout(
    title="Average Discounted Quantity Delivered",
    autosize=True,
    margin=dict(
        l=65,
        r=50,
        b=65,
        t=90
    ),
    scene=dict(
        xaxis=dict(
            title="G Tilde"
        ),
        yaxis=dict(
            title="G Bar"
        ),
        zaxis=dict(
            title="Avg. Disc. Quantity Delivered"
        )
    )
)
fig = go.Figure(data=surf, layout=layout)
py.offline.plot(fig, filename="QHat3D.html")

# Plot Payoffs
surf = [go.Surface(
    colorscale='Viridis',
    x=g_grid,
    y=g_grid,
    z=bounds
)]
layout = go.Layout(
    title="Equilibrium Payoffs",
    autosize=True,
    margin=dict(
        l=65,
        r=50,
        b=65,
        t=90
    ),
    scene=dict(
        xaxis=dict(
            title="G Tilde"
        ),
        yaxis=dict(
            title="G Bar"
        ),
        zaxis=dict(
            title="Avg. Disc. Payoff"
        )
    )
)
fig = go.Figure(data=surf, layout=layout)
py.offline.plot(fig, filename="Payoff3D.html")
