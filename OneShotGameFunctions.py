# Intercept of demand curve.
r = 10.0
# Marginal cost of quantity.
c = 1.0
# Cost of violence.
eta = 0.02
# Number of traffickers to kill.
kappa = 0.3
# The mixing threshold
mix_thresh = ((1/9) * (r - c) ** 2 + eta - c * kappa) / c
# Precision Constant
epsilon = 0.00000000000001


def calculate_nash_alpha(g, alpha, lb, ub):
    payoff = calculate_payoff(g, alpha)
    if g <= mix_thresh:
        return 1.0
    if (ub - lb) <= epsilon:
        return alpha
    if payoff < 0:
        return calculate_nash_alpha(g, (lb + ub) / 2, lb, alpha)
    elif payoff > 0:
        return calculate_nash_alpha(g, (lb + ub) / 2, alpha, ub)
    else:
        return alpha


def calculate_q(g, alpha):
    if alpha == 0:
        return 0
    else:
        return (r - c + (2 * g) + (alpha * (g + 3 * kappa))) / (2 + alpha)


def calculate_payoff(g, alpha):
    qi = calculate_q(g, alpha)

    # Calculate the utility of the case where both Cartels are 'In'
    in_price = r - 2 * (qi - g - kappa)
    in_utility = in_price * (qi - g - kappa) - c * qi - eta
    in_utility = in_utility * alpha

    # Calculate the utility of the case where one Cartel is In and the other is
    # 'Out'
    out_price = r - qi + g
    out_utility = out_price * (qi - g) - c * qi
    out_utility = out_utility * (1 - alpha)
    return in_utility + out_utility
